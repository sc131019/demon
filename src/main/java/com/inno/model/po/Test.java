package com.inno.model.po;

import lombok.Data;

@Data
public class Test {

    private Integer test1;
    private String test2;
}
